/****************************************************/
/* Aldebaran Behavior Complementary Development Kit */
/* ConnectionHtmlChoregraphe: Connection.js         */
/* Innovation - Protolab - mcaniot@aldebaran.com    */
/* Aldebaran Robotics (c) 2016 All Rights Reserved. */
/* This file is confidential.                       */
/* NOTE: THIS FILE IS PUBLISHED ONLINE               */
/****************************************************/

/* Create a session for the connection */
var session = new QiSession();

/* This function allow to connect with ALMemory thank to the box "Raise Event".
You need to give the key "PepperQiMessaging/totablet"*/
function startSubscribe() {
    session.service("ALMemory").done(function (ALMemory) {
        ALMemory.subscriber("PepperQiMessaging/totablet").done(function(subscriber) {
            subscriber.signal.connect(toTabletHandler);
        });    
    });
}

/* Receive the data send by choregraphe with the id "command". 
You can change the name of the id.*/ 
function toTabletHandler(value) { 
    // get the data and put it in the id "command"
    //document.getElementById("count").value= value;
    //tmp = document.getElementById("command").value;
    // send the data to html page
    //document.getElementById("count").innerHTML=value;
	//int count = parseInt(value);	
	//document.getElementById("debug").innerHTML = String(value);
	if (value>0) {
		document.getElementById("count1").style.background = '#03E8FF';
	} else {
		document.getElementById("count1").style.background = '#000000';
	}
	if (value>1) {
		document.getElementById("count2").style.background = '#03E8FF';
	} else {
		document.getElementById("count2").style.background = '#000000';
	}
	if (value>2) {
		document.getElementById("count3").style.background = '#03E8FF';
	} else {
		document.getElementById("count3").style.background = '#000000';
	}
	if (value>3) {
		document.getElementById("count4").style.background = '#03E8FF';
	} else {
		document.getElementById("count4").style.background = '#000000';
	}
	if (value>4) {
		document.getElementById("count5").style.background = '#03E8FF';
	} else {
		document.getElementById("count5").style.background = '#000000';
	}
    // process data with the function choice()
}

/* Send information to choregraphe thank to the event "PepperQiMessaging/fromtablet".
You need to create this event in choregraphe (add event from ALMemory).*/
function sendToChoregraphe(response) {
    session.service("ALMemory").done(function (ALMemory) {
        console.log("ALMemory");
        ALMemory.raiseEvent("PepperQiMessaging/fromTabletResponse", response);
    });
}
function interruptSpeech() {
    session.service("ALMemory").done(function (ALMemory) {
        console.log("ALMemory");
        ALMemory.raiseEvent("PepperQiMessaging/interruptSpeech", 1);
    });
}
