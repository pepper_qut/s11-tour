<?xml version="1.0" encoding="UTF-8" ?>
<Package name="PepperTour" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
    </Dialogs>
    <Resources>
        <File name="HarveyScript" src="html/HarveyScript.txt" />
        <File name="CartmanScript" src="html/CartmanScript.txt" />
        <File name="style" src="html/css/style.css" />
        <File name="About" src="html/images/About.png" />
        <File name="BlueCircle" src="html/images/BlueCircle.png" />
        <File name="Cafe" src="html/images/Cafe.png" />
        <File name="Clock" src="html/images/Clock.png" />
        <File name="Directions" src="html/images/Directions.png" />
        <File name="Exit" src="html/images/Exit.png" />
        <File name="Feedback" src="html/images/Feedback.png" />
        <File name="Handwash" src="html/images/Handwash.png" />
        <File name="Parking" src="html/images/Parking.png" />
        <File name="RedSquare" src="html/images/RedSquare.png" />
        <File name="Rights" src="html/images/Rights.png" />
        <File name="Smoking" src="html/images/Smoking.png" />
        <File name="Worried" src="html/images/Worried.png" />
        <File name="index" src="html/index.html" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="rv_logo_colour" src="html/images/rv_logo_colour.png" />
        <File name="AQLogo" src="html/images/AQLogo.PNG" />
        <File name="RoundLogo" src="html/images/RoundLogo.png" />
        <File name="RoundLogoRaised" src="html/images/RoundLogoRaised.png" />
        <File name="AppIcon" src="AppIcon.png" />
        <File name="icon" src="icon.png" />
        <File name="AboutPepper" src="html/AboutPepper.txt" />
        <File name="README" src="README.md" />
        <File name="continue" src="html/continue.html" />
        <File name="InProgress" src="html/InProgress.jpg" />
        <File name="FastForward" src="html/images/FastForward.png" />
        <File name="FastForwardRed" src="html/images/FastForwardRed.png" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_enu" src="behavior_1/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
    </Topics>
    <IgnoredPaths>
        <Path src="AppIcon.png" />
    </IgnoredPaths>
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
