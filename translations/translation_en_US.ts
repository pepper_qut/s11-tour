<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Arrived</name>
        <message>
            <source>Hello.</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello.</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Here we are. Please go on inside.</source>
            <comment>Text</comment>
            <translation type="unfinished">Here we are. Please go on inside.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Cartman</name>
        <message>
            <source>Here I talk about Cartman</source>
            <comment>Text</comment>
            <translation type="obsolete">Here I talk about Cartman</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Greet</name>
        <message>
            <source>Hello.</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Greet (1)</name>
        <message>
            <source>OK, let's go.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK, let's go.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Harvey</name>
        <message>
            <source>Here I talk about Harvey</source>
            <comment>Text</comment>
            <translation type="obsolete">Here I talk about Harvey</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Lets Go</name>
        <message>
            <source>OK, let's go.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK, let's go.</translation>
        </message>
        <message>
            <source>Let's go.</source>
            <comment>Text</comment>
            <translation type="obsolete">Let's go.</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>OK. Let's go.</source>
            <comment>Text</comment>
            <translation type="unfinished">OK. Let's go.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Lets go</name>
        <message>
            <source>OK, let's go.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK, let's go.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Lets go (1)</name>
        <message>
            <source>Let's move on.</source>
            <comment>Text</comment>
            <translation type="obsolete">Let's move on.</translation>
        </message>
        <message>
            <source>OK, let's go.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK, let's go.</translation>
        </message>
        <message>
            <source>Oops. There's a problem with my network. Plese try again in a moment.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oops. There's a problem with my network. Plese try again in a moment.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Lets go (2)</name>
        <message>
            <source>Oh dear. I seem to have a problem with the network.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh dear. I seem to have a problem with the network.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Lets go on</name>
        <message>
            <source>Let's move on.</source>
            <comment>Text</comment>
            <translation type="obsolete">Let's move on.</translation>
        </message>
        <message>
            <source>When you're ready, select continue on my screen to move on.</source>
            <comment>Text</comment>
            <translation type="obsolete">When you're ready, select continue on my screen to move on.</translation>
        </message>
        <message>
            <source>When you're ready, select next on my screen to move on.</source>
            <comment>Text</comment>
            <translation type="obsolete">When you're ready, select next on my screen to move on.</translation>
        </message>
        <message>
            <source>When you're ready, select next on my screen to move. on.</source>
            <comment>Text</comment>
            <translation type="obsolete">When you're ready, select next on my screen to move. on.</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>When you're ready, select next on my screen to continue the tour.</source>
            <comment>Text</comment>
            <translation type="unfinished">When you're ready, select next on my screen to continue the tour.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Lets go on (1)</name>
        <message>
            <source>Let's make our way to the meeting room.</source>
            <comment>Text</comment>
            <translation type="obsolete">Let's make our way to the meeting room.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Lets go on (2)</name>
        <message>
            <source>Well, here we are.</source>
            <comment>Text</comment>
            <translation type="obsolete">Well, here we are.</translation>
        </message>
        <message>
            <source>Here we are. Please go on inside.</source>
            <comment>Text</comment>
            <translation type="obsolete">Here we are. Please go on inside.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Meeting Room</name>
        <message>
            <source>Let's make our way to the meeting room.</source>
            <comment>Text</comment>
            <translation type="obsolete">Let's make our way to the meeting room.</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>OK. Let's make our way to the meeting room.</source>
            <comment>Text</comment>
            <translation type="unfinished">OK. Let's make our way to the meeting room.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Network problem</name>
        <message>
            <source>Oh dear. I seem to have a problem with the network.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh dear. I seem to have a problem with the network.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Problem</name>
        <message>
            <source>Oops. I'm not connected to the network. This won't work.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oops. I'm not connected to the network. This won't work.</translation>
        </message>
        <message>
            <source>Oops. There's a problem with my network.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oops. There's a problem with my network.</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Oops. There's a problem with my network. But let's try anyway.</source>
            <comment>Text</comment>
            <translation type="unfinished">Oops. There's a problem with my network. But let's try anyway.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Tour</name>
        <message>
            <source>Great. I like taking tours of the lab.</source>
            <comment>Text</comment>
            <translation type="obsolete">Great. I like taking tours of the lab.</translation>
        </message>
        <message>
            <source>Great. I like tours. Just give me a second to check my network connection.</source>
            <comment>Text</comment>
            <translation type="obsolete">Great. I like tours. Just give me a second to check my network connection.</translation>
        </message>
        <message>
            <source>Great. I like tours. First give me a second to check my network connection.</source>
            <comment>Text</comment>
            <translation type="obsolete">Great. I like tours. First give me a second to check my network connection.</translation>
        </message>
        <message>
            <source>Great. I like tours. First, please give me a second to check my network connection.</source>
            <comment>Text</comment>
            <translation type="obsolete">Great. I like tours. First, please give me a second to check my network connection.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Tour Intro</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Great. I like tours. First, please give me a second to check my network connection.</source>
            <comment>Text</comment>
            <translation type="unfinished">Great. I like tours. First, please give me a second to check my network connection.</translation>
        </message>
    </context>
</TS>
